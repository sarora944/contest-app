import React from 'react';
/*

 * props.showModal =  needs to pass from the calling component, on the basis of boolean, modal will show or hide

 * props.closeModalHandler = needs to pass the closeModalHandler to update the value of showModal props in the calling Component

 * props.title = this will show the Title of The Modal

 *  props.children =  This is used to display the content passed between the opening and closing of the modal tag

 */

 

const Modal = (props) => {

    return (

        props.showModal ?

            <>

                <div onClick={props.closeModalHandler} className="modal-background" />

                <div className="container">

                    <div className="modal-wrapper">

                        <span

                            className="modal-cross"

                            onClick={props.closeModalHandler}>X</span>

                        <div className="modal-head">

                            <h1>{props.title}

                            </h1>

                        </div>

                        <div>

                            {props.children}

                        </div>

                    </div>

                </div>

            </>

            : null

    )

}

 

Modal.defaultProps = {

    title: "Title"

}
export default Modal;