import React, { useState } from 'react';
import './Accordion.css';
/*
* Accordion Component will take input as an Array
* data - it will store an array
*/
const Accordion = (props) => {
    const [showAccordion, setShowAccordion] = useState(false);
    // const [data, setData] = useState(null)
    const showAccordionHandler = (index) => {
        // let updatedObj = [...data];
        // updatedObj.map((obj, i) => {
        //     if (i === index) {
        //         obj.showModal = !obj.showModal;
        //     }
        //     else {
        //         obj.showModal = false;
        //     }
        // })
        // setData(updatedObj);
        setShowAccordion(!showAccordion)
    }
    // UseEffect is used here as a ComponentWillReceiveProps, it will store in the "Data"
    // useEffect(() => {
    //     let updatedObj;
    //     if (props.input && props.input.length > 0) {
    //         updatedObj = [...props.input]
    //         updatedObj.map(obj => obj.showModal = false);
    //         setData(updatedObj)
    //     }
    // }, [props]);
    return (
        <>
            <div
                onClick={showAccordionHandler}
                className="accordion-title"
            >
                <span style={{ float: 'right', margin: "5px" }}>
                    <ion-icon
                        size="small"
                        name={showAccordion ? "remove-sharp" : "add-sharp"}
                    >
                    </ion-icon>
                </span>
                <h2>
                    {props.title} {' '}
                </h2>
            </div>
            {
                showAccordion && <div className="accordion-content">
                    <h3>
                        {props.content}
                    </h3>
                </div>
            }
        </>
    );
}
export default Accordion;