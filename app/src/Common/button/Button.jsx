import React from 'react';
import './button.css';
export const PrimaryButton = (props) => {
    return <button className="primary-button"
        onClick={props.showLoginModalHandler}>{props.children} {props.name}
    </button>
}

export const SecondaryButton = (props) => {
    return <button className="secondary-button"
        style={{color:props.color, border:`1px solid ${props.color}`}}
        onClick={props.showLoginModalHandler}>{props.children} {props.name}
    </button>
}