import React, { useState, useEffect, Fragment } from 'react';
import { PrimaryButton } from '../../Common/button/Button';
import baby1 from '../../assets/pics/baby1.jpg';
import baby2 from '../../assets/pics/baby2.jpg';
import baby3 from '../../assets/pics/baby3.jpg';
import baby4 from '../../assets/pics/baby4.jpg';
import baby5 from '../../assets/pics/baby5.jpg';
import baby6 from '../../assets/pics/baby6.jpg';
import './enteries.css';

const Enteries = () => {
    const [entriesData, setEnteriesData] = useState({
        enteries: [
            { style:{height:"250px", width:"250px"}, id:2, name:"Ishu", pic:'baby2', MCBVotes:1000, FBlikes:5000, participation:"Sep 2020" },
            { style:{height:"250px", width:"250px"}, id:3, name:"Ishu", pic:'baby3', MCBVotes:1000, FBlikes:5000, participation:"Sep 2020" },
            { style:{height:"250px", width:"250px"}, id:4, name:"Ishu", pic:'baby4', MCBVotes:1000, FBlikes:5000, participation:"Sep 2020" },
            { style:{height:"250px", width:"250px"}, id:5, name:"Ishu", pic:'baby5', MCBVotes:1000, FBlikes:5000, participation:"Sep 2020" },
            { style:{height:"250px", width:"250px"}, id:6, name:"Ishu", pic:'baby6', MCBVotes:1000, FBlikes:5000, participation:"Sep 2020" },
            { style:{height:"250px", width:"250px"}, id:1, name:"Ishu", pic:'baby1', MCBVotes:1000, FBlikes:5000, participation:"Sep 2020" },
        ]
    });

    // uncomment the code and give API url below
    // useEffect(() => {
    //     fetch('https://jsonblob.com/api/fcd4d228-6d6d-11ea-a714-b772b4c24f7e')
    //         .then(resp => resp.json())
    //         .then(resp => setEnteriesData(resp))
    // }, [])
    const ImageSelector = (img) => {
        switch (img) {
            case 'baby1':
                return baby1;
            case 'baby2':
                return baby2;
            case 'baby3':
                return baby3;
            case 'baby4':
                return baby4;
            case 'baby5':
                return baby5;
            case 'baby6':
                return baby6;
            default:
                break;
        }
    }

    const onMouseEnterHandler = (index) => {
        let updateObj = { ...entriesData };
        let style = {
            transform: "scale(1.1)",
            transition: "transform .2s",
        };
        updateObj.enteries[index].style = style
        setEnteriesData(updateObj);
    }
    const onMouseLeaveHandler = (index) => {
        let updateObj = { ...entriesData };
        let style = {
            transform: "scale(1)",
            transition: "transform .2s",
        };
        updateObj.enteries[index].style = style
        setEnteriesData(updateObj);
    }
    return (
        <div className="section-Header">
            {entriesData && entriesData.enteries && entriesData.enteries.length > 0 ? <Fragment>
                <div className="section-title">
                    <h1>PARTICIPANTS PROFILES</h1>
                    <p> PARTICIPANTS LIST FOR CURRENT CONTEST, USE SEARCH OPTION ABOVE TO FIND ANY ENTRY</p>
                </div>
                <div className=" section-content">
                    <div className="row">
                        {entriesData.enteries.map(({ style, id, name, pic, MCBVotes, FBlikes, participation }, index) => {
                            return <div key={id} className="col col-md individual-item">
                                <h2>
                                    <span>
                                        <ion-icon size="large" name="image-sharp"></ion-icon>
                                    </span>{' '}
                                    Profile 
                                </h2>
                                <img
                                    onMouseEnter={(e) => onMouseEnterHandler(index)}
                                    onMouseLeave={(e) => onMouseLeaveHandler(index)}
                                    style={style}
                                    className="contestant-pic"
                                    src={ImageSelector(pic)}
                                    alt={name + 'pic'} />
                                <h4>
                                    <ion-icon name="person-sharp" />
                                    {' '}Name : <span style={{ color: '#FA8072' }}>{name}</span></h4>
                                <h4>
                                    <ion-icon name="people-sharp" />
                                    {' '}Participation : <span style={{ color: '#FA8072' }}>{participation}</span></h4>
                                <h4>
                                    <ion-icon name="star-half-sharp" />
                                    {' '}My Cute Baby Votes : <span style={{ color: '#FA8072' }}>{MCBVotes}</span></h4>
                                <h4>
                                    <ion-icon name="logo-facebook" />
                                    {' '} FB likes : <span style={{ color: '#FA8072' }}>{FBlikes}</span></h4>
                                <PrimaryButton>VOTE</PrimaryButton>{' '}
                                <PrimaryButton>SHARE</PrimaryButton>
                            </div>
                        })}
                    </div>
                </div>
            </Fragment> :
                <div className="loader" />
            }
        </div>
    )
}

export default Enteries;