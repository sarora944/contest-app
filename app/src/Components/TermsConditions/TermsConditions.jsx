import React, { useState, useEffect } from 'react';

const TermsConditions = () => {
    const [termData, setTermData] = useState({
        terms: `Read this page carefully – it contains important rules that users need to abide by in order to take part in contest.. Please be aware that users who do not comply with these rules,T&C are at risk of losing the right to access and use the mycutebaby.in area and this Website and may also face further legal action in serious cases. This may include our co-operation with law enforcement agencies and other authorities if necessary. Remember that user activity is traceable and not anonymous. Online actions may have offline consequences.Any queries should be sent by email to query@mycutebaby.in

        mycutebaby.in recommends that users(specially Participants/voters/visitors) read these Terms and Conditions (“Terms of Service”, “Terms”) carefully prior to accessing the Website/Application. Any usage of the Website/Application by the Participant shall automatically mean his/her acceptance to these Terms and Conditions. By impliedly or expressly accepting these Terms, privacy policy, you also accept and agree to be bound by rules, policies and Terms of mycutebaby as posted on this Website from time to time and corresponding hyperlinks if any. If you do not agree to be bound by the TOS, please do not access this Website or avail any Services provided through this Website.
        
        This Activity will be open to all Users subject to the applicable terms and conditions of mycutebaby.in , or otherwise.`,
        entry: {
            title: "ENTRY REQUIREMENTS:",
            content: [
                `You must be a registered member of the MycuteBaby website to enter the competition (www.MycuteBaby.in). To enter the competition, you must be fill the Registration form with valid Details . You must upload a photo to the website using the service provided at the with registration form. By uploading a photo you confirm that you have read and agree to our Terms & Conditions and your child's profile will be available to view online. All submissions for entries to the MycuteBaby.in Promotion must be supported by explicit permission of a parent or guardian over 18 years of age of that child to allow reproduction of the image on the website.`,
                `Entrants must hold all rights for visual content uploaded as image owner (parent/guardian) or by holding written consent provided to you by the owner (eg: a professional photographer).`,
                `All entrants must be under 12 years old on the date they are entered.`,
                `No entry may contain defamatory content, including but not limited to, images, words or symbols that are widely viewed as offensive to persons of a certain ethnic, religious, racial, sexual - orientated or socioeconomic group.`,
                `Only one entry per person per competition will be eligibility for prizes.`,
                `The mycutebaby competition will run monthly and the dates for competitions follow the calendar months. Only electronic entry forms received by the closing date will be submitted to the competition. The Promoter shall be under no obligation to include your entry in the competition and shall incur no liability for emails that are late or go astray.`
            ]
        },
        generalCondition: {
            title: "GENERAL CONDITIONS OF USE AND CONTEST GUIDELINES :",
            content: [
                `It's a free contest , entries are open for all Kid's up to 12 years old.`,
                `You may participate in as many contests as you wish!`,
                `you can vote Every 30 minutes for any participant without any daily or monthly limit.`,
                `You can vote Every 30 minutes from the same INTERNET connection or from the same account`,
                `When a participant places 1st-3rd in a contest, he/she is not allowed to participate in another contest for next 2 month.`,
            ]
        },
        winner: {
            title: "WINNER CRITERIA:",
            content: [
                `Only Internet users and members have the possibility to vote for the Participants of their choice As It's an online competition .
                Winners will be Selected on Basis of Final votes . Participants having obtained the maximum number of Final Votes will be considered winners of the contest. In case of an equal number of votes among the winners, the prize will be decided by a draw.
                final votes will be counted as follow :`,
                `Final Votes = Mycutebaby Votes + (Facebook Likes & Shares /5)
                Winners will be announced online on the "Result" , "Winners" page on the website mycutebaby.in at the end of each contest.`
            ]
        },
        legal: {
            title: "LEGAL DISCLAIMER",
            content: `Use of the provided service is strictly reserved for personal use only. The use of the Site in accordance with these terms and conditions is a legal agreement between us. Any reproduction and/or representation of any element of the website in any medium is prohibited. This document is an electronic record in terms of the Information Technology Act, 2000 ("IT Act") and rules thereunder, and is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries Guidelines) Rules 2011, which mandates for publishing of rules and regulations, privacy policy and terms of use for access or usage of the Website. This electronic record is generated by a computer system and does not require any physical or digital signatures. Failure to comply with this prohibition constitutes an infringement which may result in civil and criminal liability of the offender. The information contained on this site is not contractual. It can be changed without notice.`
        },
    });

    // uncomment the code below for API URL
    // useEffect(() => {
    //     fetch('https://jsonblob.com/api/7ad5b853-7081-11ea-a3d1-ed36033eb32b')
    //         .then(resp => resp.json())
    //         .then(resp => setTermData(resp))
    // }, []);
    return (
        termData ?
            <div className="section-Header">
                <div className="section-title">
                    <h1>Kindly Read All Terms & Conditions Very Carefully</h1>
                </div>
                <div className=" section-content">
                    <div className="row">
                        <div className="col col-md individual-item">
                            <h4>{termData.terms}</h4>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-md individual-item">
                            <h2>{termData.generalCondition.title}</h2>
                            {termData.generalCondition.content.map((item, index) => (
                                <h4 key={index}>
                                    <ion-icon name="star-half-sharp"></ion-icon>{' '}
                                    {item}
                                </h4>
                            ))}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-md individual-item">
                            <h2>{termData.entry.title}</h2>
                            {termData.entry.content.map((item, index) => (
                                <h4 key={index}>
                                    <ion-icon name="star-half-sharp"></ion-icon>{' '}
                                    {item}
                                </h4>
                            ))}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-md individual-item">
                            <h2>{termData.winner.title}</h2>
                            {termData.winner.content.map((item, index) => (
                                <h4 key={index}>
                                    <ion-icon name="star-half-sharp"></ion-icon>{' '}
                                    {item}
                                </h4>
                            ))}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-md individual-item">
                            <h2>{termData.legal.title}</h2>
                            <h4 >
                                <ion-icon name="star-half-sharp"></ion-icon>{' '}
                                {termData.legal.content}
                            </h4>
                        </div>
                    </div>
                </div>
            </div> :
            <div className="loader" />
    );
}
export default TermsConditions;