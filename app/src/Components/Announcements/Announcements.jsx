import React, { Fragment, useState, useEffect } from 'react';
import { PrimaryButton } from '../../Common/button/Button';

const Announcements = () => {
    const [announcementData, setAnnouncementData] = useState({
        title:"AUG 2020",
        update:`12-09-2020 Mycutebaby Aug 2020 contest final results has been declared, click on 'My Profile/Login' button(My Profile/Login button is on the top right side of the page), use registered mobile number to login & then click on Results button, participation certificate digital copy is also available to download for eligible participants (Participation certificate for all the participants whose mycutebaby votes are more than 1000, mycutebaby votes are different from final votes).
        Gift Hampers & Vouchers has been sent to winners.
        kindly Click Here or click on below link & upload certificate pic in comments section also write your overall experience .`,
    });

    // uncomment the below code with API URL
    // useEffect(() => {
    //     fetch('https://jsonblob.com/api/a147926a-6d68-11ea-a714-cd16d310b1e3')
    //         .then(resp => resp.json())
    //         .then(resp => setAnnouncementData(resp))
    // }, [])
    return (
        <div className="section-Header">
            {announcementData ? <Fragment>
                <div className="section-title">
                    <h1>CONTEST NEWS AND UPDATES</h1>
                </div>
                <div className=" section-content">
                    <div className="row">
                        <div className="col individual-item">
                            <h2>
                                <span>
                                    <ion-icon size="large" name="newspaper-sharp"></ion-icon>
                                </span>{' '}
                                {announcementData.title}
                            </h2>
                            <h4>{announcementData.update}</h4>
                        </div>
                    </div>
                    <PrimaryButton>Participate Now</PrimaryButton>
                </div>
            </Fragment> :
                <div className="loader" />
            }
        </div>
    );
}

export default Announcements;