import React, { useState, useEffect } from 'react';
import Accordion from '../../Common/Accordion/Accordion';

const FAQ = () => {
    const [FaqData, setFaqData] = useState({
        faq: [
            {
                question: `WHAT IS ELIGIBILITY CRITERIA FOR "CUTEBABY" BABY PHOTO CONTEST? AND HOW TO PARTICIPATE INTO THIS COMPETITION`,
                answer: `"Cutebaby" Baby Photo Contest is free and will always be free to participate kids up to 12 yrs old are allowed To participate into this contest. Parents can register their baby profile into Mycutebaby competition any time in any month as long as registrations are open.                `
            },
            {
                question:`HOW TO CHECK CURRENT RANK/POSITION ?`,
                answer:`Participants can check their Current Rank/Position(if participant Mycutebaby Votes are more than 500, keep in mind mycutebaby votes are different from final votes), Tap on 'My Profile/Login' button on entry page (My Profile/Login button is on the top left side of the page) then tap on view rank button, rank will be display below participant image.`
            },
            {
                question:`HOW TO CHANGE PARTICIPANT PICTURE AND INTRO MESSAGE ?`,
                answer:`Participant Pic & Message(message displayed about participant on entry page) can be changed anytime, Tap on 'My Profile/Login' button (My Profile/Login button is on the top left side of the page), then Tap on 'Verify Account' button and login using facebook OTP(one time password) may be sent to you email id if needed, use that OTP to verify your account, on successful verification two new button will be visible in your mycutebaby dashboard below verified entry TAP on 'change pic ' or 'change message' button to update pic or msg , use google chrome or mozilla firefox browser for better experience .`
            },
            {
                question:`VOTES ARE NOT INCREASING AFTER TAPPING ON "TAP TO VOTE" BUTTON ?`,
                answer:`always use Google chrome or mozilla firefox browser to vote, if still not able to vote or getting any error msg then contact us on our fb page or email id query@cutebaby.com (use fb page for asap response).`
            },
            {
                question:`HOW TO FIX TYPO MISTAKE IN PARTICIPANT NAME (NAME CORRECTION) ?`,
                answer:`If there is a typo mistake in participant name kindly send a Name Correction email from your registered emails id to our support email id query@cutebaby.com using format below
                Subject-Name Correction
                Message- Mobile number, Participant name with correction.`
            }
        ]
    });

    // Uncomment the below code with API URL
    // useEffect(() => {
    //     fetch('https://jsonblob.com/api/3a8e653b-6e9e-11ea-afd4-9f61104a8b85')
    //         .then(resp => resp.json())
    //         .then(resp => setFaqData(resp))
    // }, [])
    return (
        FaqData ?
            <div className="section-Header">
                <div className="section-title">
                    <h1>FREQUENTLY ASKED QUESTIONS</h1>
                    <p>Kindly read contest rules and terms very carefully.</p>
                </div>
                <div className=" section-content">
                    <div className="row">
                        <div className="col col-md individual-item">
                            {FaqData && FaqData.faq && FaqData.faq.length > 0 &&
                                FaqData.faq.map(({ question, answer }, index) => {
                                    return <Accordion key={index}
                                        title={question}
                                        accordionNumber={index}
                                        content={answer}>
                                    </Accordion>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div> :
            <div className="loader" />
    )
}

export default FAQ;