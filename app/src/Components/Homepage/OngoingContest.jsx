import React, { useState, useEffect } from 'react';
const HomepageOngoingContest = () => {
    const [data, setData] = useState({
        participation:[
            {title:"SEP 2020",
            content: "Registration For MyCuteBaby September 2020 Contest are open till 30, Sep 2020, Voting will continue till 07th Oct 2020(11:59:59PM, IST), Results will be declared on or before 12th Oct 2020(11:45PM, IST)."},
            {title:"AUG 2020",
            content:"Mycutebaby Aug 2020 contest final results has been declared, tap on 'My Profile/Login' button and login using registered mobile number and then tap on Result:Aug 2020 button. participation certificate digital copy is also available to download in profile dashboard for eligible participants, kindly tap only once on 'download certificate' button to download certificate. participation certificate authenticity can be verified online by scaning the QR code displayed on certificate."},
            {title:"JUL 2020",
            content:"MyCuteBaby Jul 2020 Contest Result has been announced, certificate is also available to download for all eligible participants in mycutebaby dashboard , prizes has been sent to winners on 25th Aug 2020."},
        ]
    });

    // add the API url below for ongoing contest

    // useEffect(() => {
    //     fetch('https://jsonblob.com/api/2b2f159d-6f64-11ea-afda-e3f934b12f25')
    //     .then(resp => resp.json())
    //     .then(resp => setData(resp))
    // }, [])
    return (
        data && data.participation && data.participation.length > 0 ?
            <div className="section-Header">
                <div className="section-title">
                    <h1>ONGOING CONTESTS</h1>
                    <h2> Win exciting prices for all competition</h2>
                </div>
                <div className=" section-content">
                    <div className="row">
                        {data.participation.map(({ title, content }, index) => {
                            return <div key={index} className="col-md individual-item">
                                <h2>
                                    <span>
                                        <ion-icon name="trophy-outline">
                                        </ion-icon>
                                    </span>{'  '}
                                    {title}
                                </h2>
                                <h4>{content}</h4>
                            </div>
                        })}
                    </div>
                </div>
            </div> :
            <div className="loader" />
    );
}
export default HomepageOngoingContest;