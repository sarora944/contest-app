import React from 'react';
// import homepic from '../../assets/pics/homepage-pic.jpg';
import './homepage.css';
import HomepageOngoingContest from './OngoingContest';
import { SecondaryButton } from '../../Common/button/Button';

const Homepage = () => {
    return (
        < >
            <div className="homepage-image-container">
                <div className="homepage-img" />
                <div className="homepage-image-content">
                    <p>PHOTO CONTEST</p>
                    <SecondaryButton
                    color={"black"}>Participate</SecondaryButton>
                </div>
            </div>
            <HomepageOngoingContest />
        </>
    )
}
 
export default Homepage;