import React, { useState, useEffect, Fragment } from 'react';
import { PrimaryButton } from '../../Common/button/Button';

const Prizes = () => {
    const [prizesDetails, setPrizesDetails] = useState({
        prizes:[
            {
                position:"1",
                prize:"Gift Hampers from Cutebaby organisers."
            },
            {
                position:"2",
                prize:"Gift Hampers from Cutebaby organisers."
            },
            {
                position:"3",
                prize:"Gift Hampers from Cutebaby organisers."
            },
            {
                position:"4",
                prize:"	₹2000 Amazon shopping voucher."
            },
            {
                position:"5",
                prize:"	₹1500 Amazon shopping voucher."
            },
            {
                position:"6",
                prize:"	₹1300 Amazon shopping voucher."
            },
            {
                position:"7",
                prize:"	₹1000 Amazon shopping voucher."
            },
            {
                position:"8",
                prize:"	₹500 Amazon shopping voucher."
            },
            {
                position:"9",
                prize:"₹500 Amazon shopping voucher."
            },
            {
                position:"10",
                prize:"₹500 Amazon shopping voucher."
            },
        ]
    })

    // uncomment the below code with API URL
    // useEffect(() => {
    //     fetch("https://jsonblob.com/api/de702d30-6d56-11ea-9b50-e9e7afa92a9f")
    //         .then(data => data.json())
    //         .then(resp => setPrizesDetails(resp)
    //         )
    // }, [])
    return (
        <div className="section-Header">
            {prizesDetails && prizesDetails.prizes && prizesDetails.prizes.length > 0 ?
                <Fragment>
                    <div className="section-title">
                        <h1>PHOTO CONTEST PRIZES DETAILS</h1>
                        <h2> Mar 2020 Contest Prizes List.</h2>
                    </div>
                    <div className=" section-content">
                        <div className="row">
                            <div className="col individual-item">
                                <h2>
                                    <ion-icon size="large" name="heart-sharp"></ion-icon>{' '}
                                    PARTICIPATE
                                </h2>
                                <h2>
                                    Join tens of thousands of happy entrants in our fun, free competition
                                </h2>
                                <PrimaryButton >Participate Now</PrimaryButton>
                            </div>
                            <div className="col individual-item">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>Rank</th>
                                            <th>Prize</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {prizesDetails.prizes.map(({ position, prize }, index) => {
                                            return <tr key={index}>
                                                <td>{position}</td>
                                                <td>{prize}</td>
                                            </tr>
                                        })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </Fragment> :
                <div className="loader" />
            }
        </div>
    );
}
export default Prizes;