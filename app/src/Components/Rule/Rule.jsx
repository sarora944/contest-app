import React, { useState, useEffect } from 'react';

const Rule = () => {
    const [ruleData, setRuleData] = useState({
        rules:[
            `It's a free contest entries are open for all, Kid's upto 12 years old are allowed, we will count mycutebaby votes, facebook likes & shares.`,
            `Your friends and family members can vote for your kid every 30 minute @mycutebaby.in, Access to vote will be controlled by a countdown timer to the next opportunity to vote.`,
            `Voting for Mycutebaby Sep 2020 contest will continue till 07th Oct 2020(11:59:59PM, IST), Results will be declared on or before 12th Oct 2020(11:45PM, IST).`,
            `Result will be display in participant profile dashboard, tap on my profile/login button to open profile dashboard.Participation certificate digital copy for eligible participants( Participation certificate for all the participants whose mycutebaby votes are more than 1000.) will be available to download once results declared in participant profile dashboard.`,
            `Participants will face multiple layers of VOTING PROTECTIONS on different levels of contest. voting protections includes facebook login, geo location, google recaptcha, single browser voting restriction.
            voting protection is mandatory for Leading participants, other participants may also face voting system protection any time also Voting protections will apply automatically upon spam voting detection.`,
            `As per contest terms 1 person 1 vote every 30 minute is allowed by all means, kindly read detailed online voting terms in contest terms & conditions. violation of contest rules and terms will lead to entry delete/block without any prior warning/notice.If we detect any suspicious activity like trying to win the contest using fake/spammed votes then entry will be blocked without any prior warning/notice and will be removed from final results.`,
            `Participant can check mycutebaby votes, facebook likes & shares in participant profile dashboard any time.(Tap on 'My Profile/Login' button and login using registered mobile number to open dashboard/profile).`,
            `Participant can check their current nearby Competitors on entry page, competitors lists auto updates after every 10 minutes, kindly wait for 10 minutes for updated competitors list.`,
            `Participant can check daily votes count in daily vote graph displayed on entry page, however participants are advised to wait till midnight for confirmed updated daily votes and final votes count, confirmed votes graph get updated daily after midnight, confirmed daily votes count can be different from current day votes count.`
        ]
    })

    // uncomment the below code with API URL
    // useEffect(() => {
    //     fetch('https://jsonblob.com/api/8a3471d3-6e16-11ea-a714-9f0ffea2badf')
    //         .then(resp => resp.json())
    //         .then(resp => setRuleData(resp))
    // }, [])
    return (
        ruleData && ruleData.rules && ruleData.rules.length > 0 ?
            <div className="section-Header">
                <div className="section-title">
                    <h1>RULES</h1>
                    <p>Kindly read contest rules and terms very carefully.</p>
                </div>
                <div className=" section-content">
                    <div className="row">
                        <div className="col col-md individual-item">
                            {ruleData.rules.map((rule, index) => {
                                return <h4 key={index}>
                                    <ion-icon name="star-half-sharp"></ion-icon>{' '}
                                    {rule}
                                </h4>
                            })}
                        </div>
                    </div>
                </div>
            </div> :
            <div className="loader" />
    );
};
export default Rule;