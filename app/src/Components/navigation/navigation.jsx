import React, { useState, Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import './navigation.css';
import '../../Common/Modal/modal.css';
import Modal from '../../Common/Modal/Modal';
import { PrimaryButton } from '../../Common/button/Button';
import styled from 'styled-components';
import SearchBar from './SearchBar';

const LoginButton = styled.button`
  background-color: #fa8072;
  color: #ffffff;
  &:hover {
    background-color: #ffffff;
    color: #fa8072;
    border: 2px solid #fa8072;
  }
`;

const SignUpButton = styled.button`
  background-color: #ffffff;
  color: #fa8072;
  &:hover {
    border: 2px solid #fa8072;
    color: #fa8072;
  }
`;

const Navigation = () => {
  const [searchButtonNavBar, setSearchButtonNavBar] = useState(false);
  const [loginModal, setLoginModal] = useState(false);
  const [email, setEmail] = useState(''); // to set Email
  const [password, setPassword] = useState(''); // to set Password
  const [loginMessage, setLoginMessage] = useState(''); // to show the login successful or fail message
  const [signUp, setSignUpHandler] = useState(false); // to change to login or sign up page
  const [showPassword, setShowPassWordCheckBox] = useState(false); // show password checkbox

  const showLoginModalHandler = (e) => {
    setSignUpHandler(false);
    setLoginModal(!loginModal);
  };
  const closeModalHandler = () => {
    setLoginModal(!loginModal);
  };
  const loginHandler = (e) => {
    if (email === 'sarora944@gmail.com' && password === 'qwerty') {
      setLoginMessage('You Correctly enter Your Details');
      // this.history.pushState(null, '/');
      e.preventDefault();
    } else if (!email || !password) {
      alert('enter your details');
    } else {
      setLoginMessage('Invalid Credentials');
    }
    e.preventDefault();
  };

  const setEmailIdHandler = (e) => {
    console.log(e.target.value);
    setEmail(e.target.value);
  };

  const setPasswordHandler = (e) => {
    console.log(e.target.value);
    setPassword(e.target.value);
  };

  const signUpHandler = () => {
    setEmail('');
    setPassword('');
    setLoginMessage('');
    setSignUpHandler(!signUp);
  };

  const showPasswordCheckBox = () => {
    setShowPassWordCheckBox(!showPassword);
  };
  const searchButtonHandler = () => {
    setSearchButtonNavBar(!searchButtonNavBar);
  };
  return (
    <>
      <nav className='nav-container navbar navbar-inverse'>
        <div className='container-fluid'>
          <div className='navbar-header'>
            <NavLink className='navbar-brand' to='/'>
              WebSiteName
            </NavLink>
          </div>
          <NavLink
            className='navLink'
            activeClassName='activeRoute'
            exact
            to='/'
          >
            Home
          </NavLink>
          <NavLink
            className='navLink'
            activeClassName='activeRoute'
            to='/prizes'
          >
            Prizes
          </NavLink>
          <NavLink
            className='navLink'
            activeClassName='activeRoute'
            to='/announcements'
          >
            Announcements
          </NavLink>
          <NavLink
            className='navLink'
            activeClassName='activeRoute'
            to='/enteries'
          >
            Enteries
          </NavLink>
          <NavLink
            className='navLink'
            activeClassName='activeRoute'
            to='/rules'
          >
            Rules
          </NavLink>
          <NavLink
            className='navLink'
            activeClassName='activeRoute'
            to='/contactus'
          >
            Contact Us
          </NavLink>
          <PrimaryButton showLoginModalHandler={showLoginModalHandler}>
            <ion-icon color='white' name='person-outline'></ion-icon> Login
          </PrimaryButton>
          <div onClick={searchButtonHandler} onMouseEnter={searchButtonHandler}>
            <ion-icon
              size='large'
              name={searchButtonNavBar ? 'close-sharp' : 'search-sharp'}
            ></ion-icon>
          </div>
        </div>
      </nav>

      <Modal
        title={signUp ? 'Sign Up' : 'Login'}
        closeModalHandler={closeModalHandler}
        showModal={loginModal}
      >
        <Fragment>
          {loginMessage ? (
            <p className='login-message'>{loginMessage}</p>
          ) : null}
          <form className onSubmit={(e) => loginHandler(e)}>
            <div className='form-group'>
              <label htmlFor='email'>Email : </label>
              <input
                onChange={(e) => setEmailIdHandler(e)}
                className='form-control form-input'
                id='email'
                type='email'
                placeholder='Enter Your Email'
                value={email}
              />
            </div>

            <div className='form-group'>
              <label htmlFor='password'>Password : </label>
              <input
                onChange={(e) => setPasswordHandler(e)}
                className='form-control form-input'
                id='password'
                type={showPassword ? 'text' : 'Password'}
                placeholder='Enter Your Password'
                value={password}
              />
            </div>

            <input
              id='show-pass-checkbox'
              type='checkbox'
              onChange={showPasswordCheckBox}
            />
            <label htmlFor='show-pass-checkbox'> Show password</label>

            {!signUp && (
              <div className='form-group'>
                <LoginButton
                  type='submit'
                  className='btn btn-lg btn-block form-input'
                >
                  Login
                </LoginButton>
              </div>
            )}
          </form>
        </Fragment>

        {!signUp ? (
          <SignUpButton
            type='button'
            onClick={signUpHandler}
            style={{ marginBottom: '30px', outline: 'none' }}
            className='btn btn-lg btn-block form-input'
          >
            Not a Member? Sign Up
          </SignUpButton>
        ) : (
          <LoginButton
            type='button'
            onClick={signUpHandler}
            style={{ marginBottom: '30px', outline: 'none' }}
            className='btn btn-lg btn-block form-input'
          >
            Sign Up
          </LoginButton>
        )}
      </Modal>

      {searchButtonNavBar && (
        <SearchBar searchButtonHandler={searchButtonHandler} />
      )}
    </>
  );
};

export default React.memo(Navigation);
