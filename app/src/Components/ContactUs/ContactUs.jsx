import React from 'react';
import styled from 'styled-components';

const LoginButton = styled.button`
    background-color: #FA8072;
    color: #ffffff;
    &:hover{
        background-color: #ffffff;
        color: #FA8072;
        border: 1px solid #FA8072
}
`;
const ContactUs = () => {
    const queryHandler = (e) => {
        e.preventDefault();
    }
    return (
        <div className="section-Header">
            <div className="section-title">
                <h1>CONTACT US</h1>
                <p>We are happy to help you with all your Queries.</p>
            </div>
            <div className=" section-content">
                <h2>Didn't find what you are Looking for?</h2>
                <p>Get in Touch!</p>
                <div className="row">
                    <div className="col col-md individual-item">
                        <h2><ion-icon name="mail-sharp"></ion-icon>
                            {' '}EMAIL US</h2>
                        <h4>For any Query Regarding contest email us @
                                 <span style={{ color: "#FA8072" }}> query.websiteName.com</span>
                        </h4>
                    </div>
                    <div className="col col-md individual-item">
                        <h2><ion-icon name="create-sharp"></ion-icon>{' '}
                                SUBMIT YOUR QUERY
                            </h2>
                        <form onSubmit={queryHandler}>
                            <div className="form-group">
                                <label htmlFor="name">Name : </label>
                                <input
                                    className="form-control form-input"
                                    id="name"
                                    type="text"
                                    placeholder="Enter Your Name"
                                    required
                                />
                                <label htmlFor="email">Email : </label>
                                <input
                                    className="form-control form-input"
                                    id="email"
                                    type="email"
                                    placeholder="Enter Your Email"
                                    required
                                />
                                <label htmlFor="comments">Comments : </label>
                                <textarea
                                    className="form-control form-input"
                                    id="comments"
                                    type=""
                                    placeholder="Enter Your Query"
                                    style={{ height: "100px" }}
                                    required
                                />
                                <LoginButton
                                    type="submit"
                                    className="btn btn-lg btn-block form-input"
                                >
                                    SUBMIT
                                    </LoginButton>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default ContactUs;