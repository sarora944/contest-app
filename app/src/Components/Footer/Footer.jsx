import React from 'react';

import './footer.css';

import { NavLink } from 'react-router-dom';

 

const Footer = () => {

    return (

        <footer className="navbar-inverse footer-container">

            <div className="container">

                <div className="row">

                    <div className="col">

                        <h1>Useful Links</h1>

                        <div className="bar" />

                        <NavLink className="footer-navLink"  to="/faq">Frequently Asked Questions</NavLink>

                        <NavLink className="footer-navLink" to="/terms">Terms & Conditions</NavLink>

                        <NavLink className="footer-navLink" to="/privacy">Privacy Policy</NavLink>

                    </div>

                    <div className="col">

                        <h1>Company</h1>

                        <div className="bar" />

                        <NavLink className="footer-navLink"  to="/">Home</NavLink>

                        <NavLink className="footer-navLink" to="/prizes">Prizes</NavLink>

                        <NavLink className="footer-navLink" to="/announcements">Announcements</NavLink>

                        <NavLink className="footer-navLink" to="/enteries">Enteries</NavLink>

                        <NavLink className="footer-navLink" to="/rules">Rules</NavLink>

                        <NavLink className="footer-navLink" to="/contactus">Contact Us</NavLink>

                    </div>

                    <div className="col social">

                        <h1>Social</h1>

                        <div className="bar" />

                        <a href="/facebook" target="blank" >

                            <i className="fa fa-facebook"></i>

                        </a>

                        <a href="/instagram" target="blank">

                            <i className="fa fa-instagram"></i>

                        </a>

                        <a href="/twitter" target="blank">

                            <i className="fa fa-twitter"></i>

                        </a>

                    </div>

                </div>

 

            </div>

 

            <div className="footer-copy">© {(new Date()).getFullYear()} Copyright:

    <a href="https://google.com/" target="blank"> WebsiteName.com</a>

            </div>

        </footer >

    )

}

export default Footer;