import React from 'react';
import './App.css';
import Homepage from './Components/Homepage/HomePage';
import Navigation from './Components/navigation/navigation';
import { Route, Switch } from 'react-router-dom';
import Footer from './Components/Footer/Footer';
import Prizes from './Components/Prizes/Prizes';
import Announcements from './Components/Announcements/Announcements'
import Enteries from './Components/Enteries/Enteries';
import Rules from './Components/Rule/Rule';
import ContactUs from './Components/ContactUs/ContactUs';
import FAQ from './Components/FAQ/FAQ';
import TermsConditions from './Components/TermsConditions/TermsConditions'

function App() {
  return (
    <div className="App">
      <Navigation />
      <Switch>
        <Route path='/' exact component={Homepage} />
        <Route path='/prizes' component={Prizes} />
        <Route path='/announcements' component={Announcements} />
        <Route path='/enteries' component={Enteries} />
        <Route path='/rules' component={Rules} />
        <Route path='/contactus' component={ContactUs} />
        <Route path='/faq' component={FAQ} />
        <Route path='/terms' component={TermsConditions} />
      </Switch>
      <Footer />
    </div>
  );
}
export default App;